############################################################
#   Monster Hunter Bingo grid generator by Sienne Carmin   #
#                        March 2021                        #
############################################################

from PIL import Image, ImageDraw, ImageFont
import random as r
import textwrap as tw
import configparser as cp
import sys

class Bingo_grid:
    def __init__(self, n_grids = 1):
        config = cp.ConfigParser()
        config.read('config.txt')
        config = config['grid']
        self.grid_image = config['grid_image']
        self.content = config['content']
        self.xi = int(config['xi'])
        self.yi = int(config['yi'])
        self.dim = int(config['dim_square'])
        self.margin = int(config['margin'])
        self.font_path = config['font_path']
        self.font_size = int(config['font_size'])


    def chose_content(self):
        '''
        Choses 24 sentences to be filled in the boxes of a grid from the list file.
        Takes in the path to the text file and outputs a list of 24 sentences.
        '''
        with open(self.content, 'r') as file:
            text = file.read().strip('\n')
        text = text.split('\n')
        return r.sample(text, 24)

    def fill_grid(self, n = 1):
        '''
        Fills the sentences chosent with chose_content in a grid and saves it to a file.
        '''
        grid = Image.open(self.grid_image)
        writing = ImageDraw.Draw(grid)
        font = ImageFont.truetype(self.font_path, self.font_size)
        content = self.chose_content()
        i = 0
        for x in range(5):
            for y in range(5):
                if not (x == 2 and y == 2):
                    writing.text((self.xi + x*self.dim + self.margin, self.yi + y*self.dim + self.margin),
                                  tw.fill(content[i], 10), fill = 'black', font = font)
                    i += 1
        #grid.show()
        grid.save(f"bingo_grid{n}.png")

##### Main ######
### Creates an instance of Bingo_grid and fills one for each bingo night participant !
### The number of participants is specified in the command line.
### The grids are saved as png files in the directory the script is in.
bingo_night = Bingo_grid()

for n in range(1, int(sys.argv[1])+1):
    bingo_night.fill_grid(str(n))
